package net.sarazan.plugin.alerts

class PluginAlertToasts(
        val titles: List<String>,
        val all: Boolean)
{
    class Builder {

        var titles = listOf<String>()
        var all = false

        fun allow(vararg titles: String) {
            this.titles = titles.toList()
        }

        fun allowAll() {
            all = true
        }

        fun build(): PluginAlertToasts {
            return PluginAlertToasts(titles, all)
        }
    }
}