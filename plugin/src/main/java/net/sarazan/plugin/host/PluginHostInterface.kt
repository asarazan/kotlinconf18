package net.sarazan.plugin.host

import android.content.Context
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.handlers.HostHandlers

abstract class PluginHostInterface {

    lateinit var context: Context
        private set
    lateinit var plugin: Plugin
        private set

    abstract val alerts: HostAlerts
    abstract val handlers: HostHandlers

    protected fun initialize(context: Context, plugin: Plugin) {
        this.context = context
        this.plugin = plugin

        alerts.initialize(this)
        handlers.initialize(this)
    }
}