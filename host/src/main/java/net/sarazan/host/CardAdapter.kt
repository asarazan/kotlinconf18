package net.sarazan.host

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import net.sarazan.plugin.cards.PluginCards
import net.sarazan.plugin.cards.PluginCardVH

class CardAdapter(val host: PluginHost) : RecyclerView.Adapter<PluginCardVH>() {

    private val cards: List<PluginCards> = host.plugins.mapNotNull { it.cards }

    override fun getItemViewType(position: Int): Int {
        return cards[position].layout()
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PluginCardVH {
        Log.d("CardAdapter", "Checking index $viewType")
        val inflater = LayoutInflater.from(parent.context)
        val cardLayout = inflater.inflate(R.layout.card_container, parent, false) as ViewGroup
        val layout = viewType // we just use the layout as the view type
        LayoutInflater.from(parent.context).inflate(layout, cardLayout, true)
        return PluginCardVH(cardLayout)
    }

    override fun onBindViewHolder(holder: PluginCardVH, position: Int) {
        cards[position].onBindViewHolder(holder)
    }
}