package net.sarazan.host

import android.content.Context
import net.sarazan.plugin.alerts.PluginAlert
import net.sarazan.plugin.host.HostAlerts
import net.sarazan.plugin.host.PluginHostInterface
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class StandardHostAlerts(val c: Context) : HostAlerts {

    override lateinit var host: PluginHostInterface

    override fun initialize(host: PluginHostInterface) {
        this.host = host
    }

    override fun show(alert: PluginAlert) {

        val plugin = host.plugin
        val validate = plugin.alerts?.findAlert(alert.id)
        if (validate == null || alert != validate)
            throw SecurityException("Alert is invalid or has been tampered with.")

        c.alert(alert.message ?: "", alert.title ?: "") {
            alert.positive?.let { btn ->
                positiveButton(btn.text) {
                    btn.action?.let { host.handlers.execute(it) }
                }
            }
            alert.neutral?.let { btn ->
                neutralPressed(btn.text) {
                    btn.action?.let { host.handlers.execute(it) }
                }
            }
            alert.negative?.let { btn ->
                negativeButton(btn.text) {
                    btn.action?.let { host.handlers.execute(it) }
                }
            }
        }.show()
    }

    override fun toast(text: String) {
        val toasts = host.plugin.alerts?.toasts ?: throw SecurityException("No permissions for toasts")
        if (!toasts.all && text !in toasts.titles)
            throw SecurityException("No permission to show that toast.")
        c.toast(text)
    }

    override fun toast(text: Int) {
        toast(c.getString(text))
    }
}